import avpy
import ctypes
import logging
import threading
import time

from qr_scanner import config, packet_wrapper

logger = logging.getLogger(__name__)

class Demuxer(object):
    def __init__(self, address):
        self._inFormatCtx = None
        self._ctxLock = threading.Lock()
        self._run = threading.Event()
        self._address = address
        self._timeoutCB = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_void_p)(self._timeout_check)
        self._timeout_time = None
        self._videoStreamId = None
        self.timeout_signal = False

    def start(self):
        logger.debug("Demuxer.start: Connecting to source.")

        self._run.set()

        self._ctxLock.acquire()
        self._inFormatCtx = avpy.av.lib.avformat_alloc_context()
        if (not self._inFormatCtx):
            logger.error("Demuxer.start: Cannot alloc input format context.")
            self._ctxLock.release()
            self._run.clear()
            return False

        # set timeout check callback
        interruptStruct = avpy.av.lib.AVIOInterruptCB()
        interruptStruct.callback = self._timeoutCB
        interruptStruct.opaque = None
        self._inFormatCtx.contents.interrupt_callback = interruptStruct

        self._set_timeout(config.DEMUXER_TIMEOUT_OPEN_INPUT)
        ret = avpy.av.lib.avformat_open_input(ctypes.byref(self._inFormatCtx), self._address.encode(), None, None)
        if (ret < 0):
            if (self.timeout_signal):
                logger.error(
                    "Demuxer.start: Could not open input. Timeout. " + str(config.DEMUXER_TIMEOUT_OPEN_INPUT) + "s.")
            else:
                logger.error("Demuxer.start: Could not open input. Ffmpeg error: " + avpy.avMedia.avError(ret).decode())
            self._ctxLock.release()
            self._run.clear()
            return False

        ret = avpy.av.lib.avformat_find_stream_info(self._inFormatCtx, None)
        if (ret < 0):
            logger.error(
                "Demuxer.start: Failed to obtain input stream information of address. Ffmpeg error: " + avpy.avMedia.avError(
                    ret).decode())
            self._ctxLock.release()
            self._run.clear()
            return False

        # set video stream id
        self._videoStreamId = None
        for i in range(self._inFormatCtx.contents.nb_streams):
            if (self._inFormatCtx.contents.streams[
                i].contents.codec.contents.codec_type == avpy.av.lib.AVMEDIA_TYPE_VIDEO):
                self._videoStreamId = i
                break

        self._ctxLock.release()

        if self._videoStreamId != None:
            logger.info("Demuxer.start: Demuxer started.")
            return True
        else:
            logger.info("Demuxer.start: Cannot find video substream. Stream has no video substream or is corrupted.")
            return False

    def stop(self):
        if (not self._run.is_set()):
            return

        logger.debug("Demuxer.stop: Stopping demuxer.")

        # stop actually running read
        self._run.clear()

        # release context
        self._ctxLock.acquire()
        if (self._inFormatCtx):
            avpy.av.lib.avformat_close_input(ctypes.byref(self._inFormatCtx))
            self._inFormatCtx = None
        self._ctxLock.release()
        logger.info("Demuxer.stop: Demuxer stopped.")

    def get_context(self):
        return (self._ctxLock, self._inFormatCtx)

    def get_video_stream_id(self):
        return self._videoStreamId

    # Is it necessary alloc new packets all the time?
    def read(self):

        packet = avpy.av.lib.AVPacket()
        packetRef = ctypes.byref(packet)

        self._ctxLock.acquire()
        if (not self._run.is_set()):
            self._ctxLock.release()
            return None

        # set timeout
        self._set_timeout(config.DEMUXER_TIMEOUT_READ_FRAME)
        ret = avpy.av.lib.av_read_frame(self._inFormatCtx, packetRef)

        if (ret != 0):
            if (self.timeout_signal):
                logger.error("Demuxer.read: Cannot read packet. Timeout. " + str(config.DEMUXER_TIMEOUT_READ_FRAME) + "s.")
            elif (self._run.is_set()):
                logger.error("Demuxer.read: Cannot read packet. Ffmpeg error: " + avpy.avMedia.avError(ret).decode())
            avpy.av.lib.av_free_packet(ctypes.byref(packet))
            self._ctxLock.release()
            return None

        wrap = packet_wrapper.PacketWrapper(packet)
        wrap.calculate_dts_time(self._inFormatCtx)
        self._ctxLock.release()

        # release original packet
        avpy.av.lib.av_free_packet(ctypes.byref(packet))

        return wrap

    def _set_timeout(self, timeS):
        self._timeout_time = time.time() + timeS
        self.timeout_signal = False

    # timeout check return 0 to continue, return 1 to stop blocking function
    def _timeout_check(self, context):

        if (not self._run.is_set()):
            return 1

        if (time.time() > self._timeout_time):
            self.timeout_signal = True
            return 1
        else:
            return 0
